<?php

namespace Drupal\domain_language_negotiation;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * The domain language service provider.
 *
 * @package Drupal\domain_language_negotiation
 */
class DomainLanguageNegotiationServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    $definition = $container->getDefinition('language.default');
    $definition->setClass(LanguageDefault::class);
  }

}
