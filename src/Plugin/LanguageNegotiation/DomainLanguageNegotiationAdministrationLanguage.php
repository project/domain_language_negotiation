<?php

declare(strict_types = 1);

namespace Drupal\domain_language_negotiation\Plugin\LanguageNegotiation;

use Drupal\administration_language_negotiation\AdministrationLanguageNegotiationConditionManager;
use Drupal\administration_language_negotiation\Plugin\LanguageNegotiation\LanguageNegotiationAdministrationLanguage;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\language\LanguageNegotiatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for identifying language from domain administration language negotiation.
 */
class DomainLanguageNegotiationAdministrationLanguage extends LanguageNegotiationAdministrationLanguage {

  /**
   * Domain Negotiator.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  private DomainNegotiatorInterface $domainNegotiator;

  /**
   * Constructs a RequestPath condition plugin.
   *
   * @param \Drupal\administration_language_negotiation\AdministrationLanguageNegotiationConditionManager $manager
   *   The current path.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
    AdministrationLanguageNegotiationConditionManager $manager,
    ConfigFactoryInterface $configFactory,
    LanguageNegotiatorInterface $languageNegotiator,
    DomainNegotiatorInterface $domainNegotiator,
    array $configuration,
    $plugin_id,
    array $plugin_definition
  ) {
    $this->conditionManager = $manager;
    $this->config = $configFactory;
    $this->languageNegotiator = $languageNegotiator;
    $this->domainNegotiator = $domainNegotiator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('plugin.manager.administration_language_negotiation_condition'),
      $container->get('config.factory'),
      $container->get('language_negotiator'),
      $container->get('domain.negotiator'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getLangcode(Request $request = NULL) {
    if (!$this->domainNegotiator->getActiveDomain()) {
      return FALSE;
    }

    return parent::getLangcode($request);
  }

}
