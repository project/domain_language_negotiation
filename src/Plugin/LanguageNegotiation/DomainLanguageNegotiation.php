<?php

namespace Drupal\domain_language_negotiation\Plugin\LanguageNegotiation;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\language\Annotation\LanguageNegotiation;
use Drupal\language\LanguageNegotiationMethodBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for identifying language based on domain language.
 *
 * @LanguageNegotiation(
 *   id =
 *   Drupal\domain_language_negotiation\Plugin\LanguageNegotiation\DomainLanguageNegotiation::METHOD_ID,
 *   weight = -1, name = @Translation("Domain language"), description =
 *   @Translation("Language based on the current domains language settings")
 * )
 */
class DomainLanguageNegotiation extends LanguageNegotiationMethodBase implements ContainerFactoryPluginInterface {

  /**
   * Negotiate language based on domain language.
   *
   * Logic:
   * If current language is available for the domain, use that.
   * Otherwise, use the domains default language.
   */
  public const METHOD_ID = 'domain-language';

  /**
   * Domain Negotiator.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  private $domainNegotiator;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Language Manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new LanguageNegotiationUserAdmin instance.
   *
   * @param \Drupal\domain\DomainNegotiatorInterface $domainNegotiator
   *   Domain Negotiator.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config Factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language Manager.
   */
  public function __construct(DomainNegotiatorInterface $domainNegotiator, ConfigFactoryInterface $configFactory, LanguageManagerInterface $languageManager) {
    $this->domainNegotiator = $domainNegotiator;
    $this->configFactory = $configFactory;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('domain.negotiator'),
      $container->get('config.factory'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getLangcode(Request $request = NULL) {
    if ($domain = $this->domainNegotiator->getActiveDomain()) {
      $name = 'domain.language.' . $domain->getOriginalId() . '.language.negotiation';
      $domain_languages = $this->configFactory->get($name)->get('languages');
      $current_language = $this->languageManager->getCurrentLanguage()->getId();

      // If current language is enabled for the domain, use that.
      if (is_array($domain_languages) && in_array($current_language, $domain_languages, TRUE)) {
        return $current_language;
      }

      // Otherwise use the domains default language.
      $config = $this->configFactory->get('domain.config.' . $domain->getOriginalId() . '.system.site')
        ->getRawData();
      if (isset($config['default_langcode'])) {
        return $config['default_langcode'];
      }
    }

    return NULL;
  }

}
