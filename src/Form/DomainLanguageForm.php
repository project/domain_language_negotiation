<?php

namespace Drupal\domain_language_negotiation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The domain language admin form.
 *
 * @package Drupal\domain_language_negotiation\Form
 */
class DomainLanguageForm extends FormBase {

  public const DEFAULT_LANGUAGE_SITE = '***LANGUAGE_site_default***';

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * The route builder.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  private $routeBuilder;

  /**
   * Constructs the domain language form.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $routeBuilder
   *   The route builder.
   */
  public function __construct(LanguageManagerInterface $languageManager, RouteBuilderInterface $routeBuilder) {
    $this->languageManager = $languageManager;
    $this->routeBuilder = $routeBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): DomainLanguageForm {
    return new static(
      $container->get('language_manager'),
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'domain_language_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $build_info = $form_state->getBuildInfo();

    /** @var \Drupal\domain\DomainInterface $domain */
    $domain = $build_info['args'][0];

    // All available languages.
    $languages = $this->languageManager->getNativeLanguages();

    // Config without any override.
    $configRaw = $this->config('system.site')->getRawData();
    $defaultRaw = $configRaw['default_langcode'];

    // Load the domain default language.
    $config = $this->config('domain.config.' . $domain->getOriginalId() . '.system.site')->getRawData();
    $defaultOldLanguage = $config['default_langcode'] ?? self::DEFAULT_LANGUAGE_SITE;

    $config = $this->config('domain.language.' . $domain->getOriginalId() . '.language.negotiation');
    $data = $config->getRawData();

    /** @var \Drupal\Core\Language\LanguageInterface $defaultLanguageRaw */
    $defaultLanguageRaw = $this->languageManager->getLanguage($defaultRaw);

    $options = [
      self::DEFAULT_LANGUAGE_SITE => $this->t(
        "Site's default language (@lang_name)",
        ['@lang_name' => $defaultLanguageRaw->getName()]
      ),
    ];
    foreach ($languages as $language) {
      $options[$language->getId()] = $language->getName();
    }

    $form['domain_id'] = [
      '#type' => 'value',
      '#value' => $domain->getOriginalId(),
    ];

    $form['default_language'] = [
      '#type' => 'select',
      '#title' => $this->t('Default language'),
      '#options' => $options,
      '#description' => $this->t(
        'This will override the default language: %default.',
        ['%default' => $defaultLanguageRaw->getName()]
      ),
      '#default_value' => $data['default_langcode'] ?? $defaultOldLanguage,
      '#required' => TRUE,
    ];

    $options = [];
    foreach ($languages as $language) {
      $options[$language->getId()] = $language->getName();
    }

    $form['languages'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Languages allowed'),
      '#description' => $this->t('If none selected, all will be available. Default language will be added automatically.'),
      '#options' => $options,
      '#default_value' => $data['languages'] ?? [],
      '#required' => FALSE,
    ];

    $form['actions'] = [
      '#type' => 'container',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save'),
    ];

    $form['actions']['cancel'] = Link::createFromRoute($this->t('Cancel'), 'domain.admin')->toRenderable();
    $form['actions']['cancel']['#attributes']['class'] = [
      'button',
      'button--danger',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $domain_id = $form_state->getValue('domain_id');
    $default_language = $form_state->getValue('default_language');
    $languages = array_filter($form_state->getValue('languages'));

    // Set default language into domain config file.
    $config = $this->configFactory()->getEditable('domain.language.' . $domain_id . '.language.negotiation');
    if ($default_language !== self::DEFAULT_LANGUAGE_SITE) {
      $config->set('default_langcode', $default_language);
      $languages[$default_language] = $default_language;
    }
    else {
      $config->clear('default_langcode');
    }

    // Set default language into domain language file.
    $config = $this->configFactory()->getEditable('domain.language.' . $domain_id . '.language.negotiation');
    if (!empty($languages)) {
      $config->set('languages', $languages);
    }
    else {
      $config->clear('languages');
    }

    if (empty($config->get('languages'))) {
      $config->delete();
      $this->routeBuilder->rebuild();
      return;
    }

    $config->clear('url.prefixes');
    $config->clear('url.domains');
    $config->save();
    $this->routeBuilder->rebuild();
  }

}
