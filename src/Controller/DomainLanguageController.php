<?php

namespace Drupal\domain_language_negotiation\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\domain_language_negotiation\Form\DomainLanguageForm;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * The domain language controller.
 *
 * @package Drupal\domain_language_negotiation\Controller
 */
class DomainLanguageController extends ControllerBase {

  /**
   * Edit domain language restrictions.
   *
   * @param mixed $domain
   *   The domain.
   *
   * @return array
   *   Edit form render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function edit($domain): array {
    $domainLoader = $this->entityTypeManager()->getStorage('domain');
    if (!$domain = $domainLoader->load($domain)) {
      throw new NotFoundHttpException();
    }

    return [
      'edit_form' => $this->formBuilder()->getForm(DomainLanguageForm::class, $domain),
    ];
  }

}
