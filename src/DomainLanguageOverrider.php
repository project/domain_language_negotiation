<?php

namespace Drupal\domain_language_negotiation;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorableConfigBase;
use Drupal\Core\Config\StorageInterface;

/**
 * The domain language overrider.
 *
 * @package Drupal\domain_language_negotiation
 */
class DomainLanguageOverrider implements ConfigFactoryOverrideInterface {

  /**
   * The domain negotiator.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * A storage controller instance for reading and writing configuration data.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $storage;

  /**
   * The domain context of the request.
   *
   * @var \Drupal\domain\DomainInterface
   */
  protected $domain;

  /**
   * The nested level.
   *
   * @var int
   */
  protected $nestedLevel;

  /**
   * Indicates that the request context is set.
   *
   * @var bool
   */
  protected $contextSet;

  /**
   * Constructs a DomainLanguageOverrider object.
   *
   * @param \Drupal\Core\Config\StorageInterface $storage
   *   The configuration storage engine.
   */
  public function __construct(StorageInterface $storage) {
    $this->storage = $storage;
    $this->nestedLevel = 0;
  }

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names): array {
    global $config;

    $this->nestedLevel++;
    $overrides = [];
    if (empty($this->contextSet)) {
      $this->initiateContext();
    }

    if ($this->domain) {
      foreach ($names as $name) {
        if ($name === 'system.site') {
          $overrider = \Drupal::service('domain_config.overrider');
          $configs = $overrider->loadOverrides([$name]);

          if (!empty($configs[$name])) {
            // Initialize site settings.
            $overrides[$name] = $configs[$name];
          }

          $languageConfigOverride = \Drupal::config('domain.language.' . $this->domain->getOriginalId() . '.language.negotiation')->getRawData();
          if (!empty($languageConfigOverride['default_langcode'])) {
            $overrides[$name]['default_langcode'] = $languageConfigOverride['default_langcode'];
          }
        }
      }
    }

    $this->nestedLevel--;

    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    $suffix = $this->domain ? $this->domain->id() : '';

    return $suffix ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION): ?StorableConfigBase {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name): CacheableMetadata {
    if (empty($this->contextSet)) {
      $this->initiateContext();
    }
    $metadata = new CacheableMetadata();
    if (!empty($this->domain)) {
      $metadata->addCacheContexts(['url.site']);
    }

    return $metadata;
  }

  /**
   * Sets domain and language contexts for the request.
   *
   * We wait to do this in order to avoid circular dependencies
   * with the locale module.
   */
  protected function initiateContext(): void {
    $this->contextSet = TRUE;

    $this->domainNegotiator = \Drupal::service('domain.negotiator');
    // Get the domain context.
    $this->domain = $this->domainNegotiator->getActiveDomain();
    // If we have fired too early in the bootstrap,
    // we must force the routine to run.
    if (empty($this->domain)) {
      $this->domain = $this->domainNegotiator->getActiveDomain(TRUE);
    }
  }

}
