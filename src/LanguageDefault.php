<?php

namespace Drupal\domain_language_negotiation;

use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageDefault as BaseLanguageDefault;
use Drupal\Core\Language\LanguageInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;

/**
 * The language default extension.
 *
 * @package Drupal\domain_language_negotiation
 */
class LanguageDefault extends BaseLanguageDefault {
  /**
   * The default language for domain.
   *
   * @var \Drupal\Core\Language\LanguageInterface
   */
  protected $languageDomain;

  /**
   * {@inheritdoc}
   */
  public function set(LanguageInterface $language): void {
    if (empty($this->languageDomain)) {
      parent::set($language);
    }
    else {
      $this->languageDomain = $language;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function get() {
    // Try while domain is loaded.
    if (!empty($this->languageDomain)) {
      return $this->languageDomain;
    }
    // Load default language.
    $this->languageDomain = parent::get();

    try {
      /** @var \Drupal\domain\DomainNegotiatorInterface $negotiator */
      $negotiator = \Drupal::service('domain.negotiator');
      $domain = $negotiator->getActiveDomain();
      if (!$domain) {
        $language = $this->languageDomain;
        unset($this->languageDomain);
        return $language;
      }

      $name = 'domain.language.' . $domain->getOriginalId() . '.language.negotiation';
      $default_langcode = \Drupal::configFactory()->get($name)->get('default_langcode');

      if (!empty($default_langcode)) {
        return $this->setLanguage($default_langcode, $this->languageDomain);
      }

      $default_langcode = \Drupal::config('system.site')->get('default_langcode');
      return $this->setLanguage($default_langcode, $this->languageDomain);
    }
    catch (ServiceCircularReferenceException $e) {
      $language = $this->languageDomain;
      unset($this->languageDomain);
      return $language;
    }
  }

  /**
   * Set the default language.
   *
   * @param string $default_langcode
   * @param LanguageInterface $fallback_language
   *
   * @return \Drupal\Core\Language\LanguageInterface
   */
  private function setLanguage(string $default_langcode, LanguageInterface $fallback_language) {
    if ($fallback_language->getId() === $default_langcode) {
      return $fallback_language;
    }

    $language = $this->getLanguage($default_langcode);

    // @todo check if necessary ?
    \Drupal::languageManager()->reset();

    // Update default language in translation service.
    if ($translation = \Drupal::translation()) {
      $translation->setDefaultLangcode($language->getId());
    }

    $this->languageDomain = $language;

    return $language;
  }

  /**
   * Gets the language.
   *
   * @param string $langcode
   *   The language code.
   *
   * @return \Drupal\Core\Language\Language
   *   The language.
   */
  protected function getLanguage(string $langcode): Language {
    $config = \Drupal::config('language.entity.' . $langcode);
    $data = $config->get();
    $data['name'] = $data['label'];

    return new Language($data);
  }

}
